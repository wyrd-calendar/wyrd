(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)

(* OCaml binding for setlocale(), required to kick ncurses into
 * properly rendering non-ASCII chars. *)

type t = LC_ALL | LC_COLLATE | LC_CTYPE | LC_MONETARY |
         LC_NUMERIC | LC_TIME | LC_MESSAGES |
         LC_UNDEFINED of int


(* Binds to C library setlocale() *)
external setlocale_int : int -> string -> string option = "ml_setlocale"


(* Provide a more OCamlish interface *)
let setlocale (category : t) (param : string) =
   let int_category =
      match category with
      | LC_ALL         -> 0
      | LC_COLLATE     -> 1
      | LC_CTYPE       -> 2
      | LC_MONETARY    -> 3
      | LC_NUMERIC     -> 4
      | LC_TIME        -> 5
      | LC_MESSAGES    -> 6
      | LC_UNDEFINED i -> i
   in
   setlocale_int int_category param
