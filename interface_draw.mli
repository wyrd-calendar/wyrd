(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


val sort_lineinfo : Interface.timed_lineinfo_t -> Interface.timed_lineinfo_t -> int

val draw_help : Interface.interface_state_t -> unit

val draw_date_strip : Interface.interface_state_t -> unit

val draw_timed :
  Interface.interface_state_t -> Remind.timed_rem_t list array -> Interface.interface_state_t

val draw_timed_try_window :
  Interface.interface_state_t -> Remind.timed_rem_t list array -> int -> int
  -> Interface.interface_state_t

val draw_calendar : Interface.interface_state_t -> Remind.three_month_rem_t -> unit

val draw_untimed :
  Interface.interface_state_t -> Remind.untimed_rem_t list -> Interface.interface_state_t

val draw_msg : Interface.interface_state_t -> Interface.interface_state_t

val draw_error : Interface.interface_state_t -> string -> bool -> unit

val draw_selection_dialog :
  Interface.interface_state_t -> string -> string list -> int -> int -> unit
